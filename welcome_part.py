        
def welcome_part() -> List[str] :
    lis = []
    print("\n"*3)
    welcom_animation()
    time.sleep(2)
    Player_1 = input(Fore.MAGENTA+"Enter host name:").upper()
    Player_2 = input(Fore.MAGENTA+"Enter player name:").upper()
    print(Fore.MAGENTA+"Host: ", Player_1)
    print(Fore.MAGENTA+"Player: ", Player_2)
    print(Fore.MAGENTA+f'Enter the secret word {Player_1}:')
    
    secret_word = getpass.getpass()
    word_completion = "__ " * len(secret_word)
    
    req_hint_or_not = input(Fore.MAGENTA+"Do you want Hint ? y/n:")
    hint = ""
    if req_hint_or_not == 'y' or req_hint_or_not == 'Y':
        hint = input(Fore.MAGENTA+" Enter hint:")
    lis.append(hint)
    lis.append(secret_word)
    lis.append(Player_1)
    lis.append(Player_2)
    return lis
