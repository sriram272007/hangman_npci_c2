import colorama
from colorama import Fore
import random
from typing import List
import time
import getpass
from IPython.display import clear_output

def playAgain():
    user_input = input(Fore.RESET+"Do you want to play again ??, y or n").lower() 
    if user_input == 'y' or user_input == 'yes':
        
        #clear screen section
        clear_output(wait=False)
        
        hangman()
    else:
        print(Fore.LIGHTRED_EX+"\n\n======================game over===================\n\n\n".center(105))
        print("here are the creater of this game : Rajat, Srikanth, sudhanshu, priya, subhashree, Abhijeet")
        
    

def hangman():
    emoji = {0:"\U0001F642",1:"\U0001F610",2:"\U0001F644",3:"\U0001F631",4:"\U0001F616",5:"\U0001F480",
            6:"\U0001F44D"}
    list_data = welcome_part()
    
    #clear screen section
    clear_output(wait=False)
    
    list_data[1] = list_data[1].upper()
    secret_word = [x for x in list_data[1]]
    hint_character_1 = random.randint(0,len(secret_word)//2)
    hint_character_2 = random.randint(len(secret_word)//2+1,len(secret_word)-1)
    game_word=list(len(secret_word) * '_')
    
    hint_char_arr = [secret_word[hint_character_1],secret_word[hint_character_2]]
    for i in hint_char_arr:
        for j in range(len(secret_word)):
            if i == secret_word[j]:
                game_word[j] = i
                  

    if len(list_data[0]) == 0:
        print(f'No hint available for this problem {list_data[3]} ')
    else:      
        print(f'Here is your hint {list_data[3]} : {list_data[0]}')
    
    wrong_guess = 0
    win = False
    while not win and wrong_guess < 5 :
        
        print(' '.join([str(s) for s in game_word]))
        
        
        if "_" not in game_word:
            win = True
            break
        time.sleep(2)
        
        guess_input = input(f'\nEnter your Guess {list_data[3]}: ').upper()
